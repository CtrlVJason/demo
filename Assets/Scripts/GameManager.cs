﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GameManager : MonoBehaviour {

	public GameObject objectToSpawn;                // The enemy prefab to be spawned.

	public void Spawning(Vector3 point, Quaternion rotation)
    {
        Instantiate(objectToSpawn, point, rotation);
    }
}
