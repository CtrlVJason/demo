﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

	public Transform target;
	public Vector3 offset = new Vector3(0, 15f, -22f);
	public Vector3 rotation = new Vector3(30, 0, 0);
	public float smoothing = 5f;

	Transform cameraTransform;

	void Awake()
	{
		cameraTransform = Camera.main.transform;
		cameraTransform.position = target.position + offset;
		cameraTransform.eulerAngles = rotation;
	}
	void FixedUpdate()
	{
		Vector3 targetCamPos = target.transform.position + offset;
		cameraTransform.position = Vector3.Lerp(cameraTransform.position, targetCamPos, smoothing * Time.deltaTime);
	}
}
