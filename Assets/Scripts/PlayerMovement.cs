﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

using Hashtable = ExitGames.Client.Photon.Hashtable;

public class PlayerMovement : MonoBehaviour
{

    public float speed = 6f;

    PhotonView photonView;
    Vector3 movement;
    Animator anim;
    Rigidbody playerRigidbody;
    int floorMask;
    float camRayLength = 100f;

    float logTime = 1f;
	float timer;                                    // A timer to determine when to fire.


    void Awake()
    {
        Debug.Log("Awake Called!");
        floorMask = LayerMask.GetMask("Floor");
        anim = GetComponent<Animator>();
        playerRigidbody = GetComponent<Rigidbody>();
        photonView = GetComponent<PhotonView>();

        if ( photonView.IsMine )
        {
            Camera.main.GetComponent<CameraFollow> ().target = this.transform;
        }
    }

    void Start()
    {
        if (photonView.Owner.CustomProperties["Color"] != null) {
            Vector4 data = (Vector3)photonView.Owner.CustomProperties["Color"];

            // Debug.Log("The Owner's Id is: " + photonView.OwnerActorNr + " And It's props are: " + photonView.Owner.CustomProperties.ToStringFull());

            data.w = 1f;

            GetComponentInChildren<Renderer>().material.color =  data;
        }
    }

    void Update() {
        if (!photonView.IsMine) {
            return;
        }

        timer += Time.deltaTime;

        if (Input.GetButton ("Fire2") && timer >= logTime) {
            ColorSwap();
        }

        if (Input.GetButton ("Fire3") && timer >= logTime) {
            ReadProperty();
        }
    }

    void FixedUpdate()
    {
        if (!photonView.IsMine) {
            return;
        }

        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");

        Move(h, v);
        Turning();
    }

    void Move(float h, float v)
    {
        movement.Set(h, 0f, v);
        movement = movement.normalized * speed * Time.deltaTime;

        playerRigidbody.MovePosition(transform.position + movement);
    }

    void Turning()
    {
        Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);

        RaycastHit floorHit;

        if (Physics.Raycast(camRay, out floorHit, camRayLength, floorMask))
        {
            Vector3 playerToMouse = floorHit.point - transform.position;
            playerToMouse.y = 0f;

            Quaternion newRotation = Quaternion.LookRotation(playerToMouse);
            playerRigidbody.MoveRotation(newRotation);
        }
    }

    void ColorSwap() {
        timer = 0;

        // Hashtable a = PhotonNetwork.LocalPlayer.CustomProperties;

        Hashtable a = new Hashtable();

        a["Color"] = new Vector3(Random.value, Random.value, Random.value);

        PhotonNetwork.LocalPlayer.SetCustomProperties(a);
    }

    void ReadProperty() {
        timer = 0;
        
        Player[] playerInRoom = PhotonNetwork.PlayerList;

        string list = "";

        foreach (Player player in playerInRoom)
		{
            list += player.NickName + ": " + player.CustomProperties.ToString() + "; \n";
		}

        Debug.Log(list);
    }

}
