﻿using UnityEngine;
using System.Net.NetworkInformation;

using Photon.Pun;
using Photon.Realtime;

using ExitGames.Client.Photon;

public class NetworkManager : MonoBehaviourPunCallbacks, IOnEventCallback
{

	[Tooltip("The game's current version. Edit this if there is a game breaking change that needs to be reflected.")]
	[SerializeField]
	private string gameVersion = "1";

	[Tooltip("The maximum number of players per room. When a room is full, it can't be joined by new players, and so new room will be created")]
	[SerializeField]
	private byte maxPlayersPerRoom = 200;

	public GameManager gameManager;

    private readonly byte SpawnCars = 0;

	// OnEvent call
    public void OnEvent(EventData photonEvent)
    {
        byte eventCode = photonEvent.Code;


        if (eventCode == SpawnCars)
        {
            Debug.Log("Recieved Spawn Cars Event: " + eventCode);

            object[] data = (object[])photonEvent.CustomData;

            Vector3 position = (Vector3)data[0];
            Quaternion rotation = (Quaternion)data[1];

            gameManager.Spawning(position, rotation);
        }
    }

	public override void OnEnable()
	{
		PhotonNetwork.AddCallbackTarget(this);
	}

	public override void OnDisable()
	{
		PhotonNetwork.RemoveCallbackTarget(this);
	}

	public string GetMachineMacAddress()
    {
		NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();

		string macIdentifier = "";

		foreach (NetworkInterface adapter in nics)
		{
			macIdentifier = adapter.GetPhysicalAddress().ToString();

			if (macIdentifier.Length > 0) {
				break;
			}
		}
		return macIdentifier;
	}

	void Awake()
	{
		// #Critical
		// this makes sure we can use PhotonNetwork.LoadLevel() on the master client and all clients in the same room sync their level automatically
		PhotonNetwork.AutomaticallySyncScene = true;
		
		Hashtable playerCustomProperty = new Hashtable();
		playerCustomProperty.Add("MacIdentifier", GetMachineMacAddress());

		PhotonNetwork.LocalPlayer.SetCustomProperties(playerCustomProperty);
	}

	void Start()
	{
		Connect();
	}

	public void Connect()
	{
		// we check if we are connected or not, we join if we are , else we initiate the connection to the server.
		Debug.Log("PhotonNetwork.IsConnected" + PhotonNetwork.IsConnected);
		if (PhotonNetwork.IsConnected)
		{
			// PhotonNetwork.
			// #Critical we need at this point to attempt joining a Random Room. If it fails, we'll get notified in OnJoinRandomFailed() and we'll create one.

			PhotonNetwork.JoinOrCreateRoom("Lobby", new RoomOptions { MaxPlayers = maxPlayersPerRoom }, Photon.Realtime.TypedLobby.Default);

			Debug.Log("Room Joined or Created");
		}
		else
		{
			// #Critical, we must first and foremost connect to Photon Online Server.
			PhotonNetwork.GameVersion = gameVersion;
			PhotonNetwork.ConnectUsingSettings();
		}
	}

	public override void OnConnectedToMaster()
	{
		Debug.Log("PUN Basics Tutorial/Launcher: OnConnectedToMaster() was called by PUN");

		// We now know that we are connected to the master server.
		Connect();
	}

	public override void OnDisconnected(DisconnectCause cause)
	{
		Debug.Log("PUN Basics Tutorial/Launcher: OnDisconnectedFromPhoton() was called by PUN");

		if (cause == DisconnectCause.ExceptionOnConnect)
		{
			PhotonNetwork.OfflineMode = true;
		}
	}

	public override void OnJoinRandomFailed(short returnCode, string message)
	{
		Debug.Log("No random room available, so we create one.\nCalling: PhotonNetwork.CreateRoom");

		// #Critical: we failed to join a random room, maybe none exists or they are all full. No worries, we create a new room.
		PhotonNetwork.CreateRoom("Lobby", new RoomOptions { MaxPlayers = maxPlayersPerRoom});
	}

	public override void OnJoinedRoom()
	{

		Debug.Log("Now this client is in a room. " + PhotonNetwork.CurrentRoom.Name + " " + PhotonNetwork.CurrentRoom.PlayerCount);

		Debug.Log("This Player To String Full is: " + PhotonNetwork.LocalPlayer.ToStringFull());

		// Now we are in a room try and spawn the player? or do a callback to spawn a player. Game manager maybe? but I am just gonna call it here for now.
		GameObject a = PhotonNetwork.Instantiate("Player", new Vector3(0f,0f,0f), Quaternion.identity, 0);

		Debug.Log("Hello World " + a.GetComponent<PhotonView> ().Owner.ActorNumber);

		Hashtable playerCustomProperty = new Hashtable();
        object[] playerDetails = new object[2];
        playerDetails[0] = "123";
        playerDetails[1] = "nullgame";
        playerCustomProperty.Add("2", playerDetails);

        PhotonNetwork.CurrentRoom.SetCustomProperties(playerCustomProperty);
	}

	public override void OnRoomPropertiesUpdate(Hashtable propertiesThatChanged) {
		Debug.Log(propertiesThatChanged.ToStringFull());
		if (propertiesThatChanged.ContainsKey("2")){
			object[] a = (object [])propertiesThatChanged["2"];
			Debug.Log((string)a[1]);
		}
	}

	public override void OnPlayerPropertiesUpdate(Player target, Hashtable changedProps)
	{
		Debug.Log("OnPlayerPropertiesUpdate() Callback Called!");

		Debug.Log("The Changed Props are: " + changedProps.ToStringFull());
		Debug.Log("The Full Props are: " + target.CustomProperties.ToStringFull());


		foreach (GameObject obj in GameObject.FindGameObjectsWithTag("Player"))
		{
			if (obj.GetComponent<PhotonView>().OwnerActorNr == target.ActorNumber)
			{
				// Vector4 data = (Vector3)changedProps["Color"];
				Vector4 data = (Vector3)target.CustomProperties["Color"];

				data.w = 1f;

				Debug.Log("Data is: " + data);

        		obj.GetComponentInChildren<Renderer>().material.color =  data;	

			}
		}
	}
}
