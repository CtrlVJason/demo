﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

using SendOptions = ExitGames.Client.Photon.SendOptions;


public class PlayerShooting : MonoBehaviour  {

	public float timeBetweenSpawns = 3f;
	float timer;                                    // A timer to determine when to fire.

    int floorMask;
    float camRayLength = 100f;
    
    PhotonView photonView;

    private readonly byte spawnCars = 0;

	// Use this for initialization
	void Awake () {
		timer = timeBetweenSpawns;
		floorMask = LayerMask.GetMask("Floor");
        photonView = GetComponent<PhotonView>();
	}

    void Update()
    {
        if (!photonView.IsMine) {
            return;
        }

		timer += Time.deltaTime;


        if(Input.GetButton ("Fire1") && timer >= timeBetweenSpawns)
        {
            // ... shoot the gun.
            SpawnCheck ();
        }
    }

    void SpawnCheck() 
    {
        Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);

        RaycastHit floorHit;

        if (Physics.Raycast(camRay, out floorHit, camRayLength, floorMask))
        {
			timer = 0;
            
            PhotonNetwork.RaiseEvent(spawnCars, new object[] { floorHit.point, transform.rotation }, new RaiseEventOptions() { Receivers = ReceiverGroup.All, CachingOption = EventCaching.AddToRoomCacheGlobal }, new SendOptions() { Reliability = true });
        }
    }

}
